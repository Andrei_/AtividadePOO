package br.ucsal;


import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Scanner;

public class ListaDeContatos {
	static Scanner sc = new Scanner(System.in);
	static int cont = 0; 
	static Estado x;
	public static void main(String[] args){

		int i = 0,opc = 0;
		System.out.println("Quantos contatos deseja nesta agenda ? ");
		i = sc.nextInt();
		Contatos[] primeiro = new Contatos[i];

		opc = menu();
		while(opc != 5) {
			if(opc == 1) {
				do {
					incluir(primeiro);

				}while(cont < i);
				opc = menu();

				while(opc == 1) {
					System.out.println("Os contatos j� foram inclusos !!!");
					opc = menu();
					System.out.println();
				}

			}
			else if(opc == 2) {
				listar(primeiro);
				opc = menu();
			}
			else if(opc == 3) {
				excluir(primeiro);

				opc = menu();
			}
			else if(opc == 4) {
				pesquisar(primeiro);

				opc = menu();
			}
		}
	}

	
	public static void incluir(Contatos[]vet) {
		int c = 0;
		
		Estado estado = Estado.PESSOAL;
		Estado estado2 = Estado.PROFISSIONAL;
		SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy");
		Integer anoNasc = 0;
		String nome = " ", telefone = " ", data = " ";
		Contatos contato = new Contatos();
		System.out.println("Informe o nome do contato que deseja incluir");
		nome = sc.next();
		contato.setNome(nome);
		System.out.println("Informe o n�mero do contato ");
		telefone = sc.next();
		contato.setTelefone(telefone);
		System.out.println("Informe o ano de nascimento");
		anoNasc = sc.nextInt();
		contato.setAnoNascimento(anoNasc);
		System.out.println("Informe a data de seu nascimento no formato [dd/MM/yyyy] ");
		data = sc.next();
		System.out.println("Qual o tipo de contato ?\n [1]Pessoal\n [2]Profissional");
		c = sc.nextInt();

		if(c == 1) {
			x = estado;
			sc.nextLine();
		}
		
		else if(c == 2) {
			x = estado2;
			sc.nextLine();
		}


		try {
			contato.setDataNasc(format.parse(data));
		} catch (ParseException e) {

			e.printStackTrace();
		}

		System.out.println();
		vet[cont] = contato;
		cont++;

	}
	private static void getContato(Contatos[] primeiro) {

		for (int j = 0; j < primeiro.length; j++) {
			if(primeiro[j] == null)continue;
			System.out.println("Contato " + (j+1));
			System.out.println("Nome: " +  primeiro[j].getNome());
			System.out.println("Telefone: " + primeiro[j].getNome());
			System.out.println("Ano de nascimento: " + primeiro[j].getAnoNascimento());
			System.out.println("Data de nascimento: " + primeiro[j].getDataNasc());
			System.out.println("Contado de tipo: " + x);
			System.out.println();
		}
	}
	public static void listar(Contatos[]vet) {
		getContato(vet);
	}
	public static void excluir(Contatos[]vet) {
		int posicao = 0;
		System.out.println("Qual contato deseja excluir ? ");
		for (int j = 0; j <vet.length; j++) {
			System.out.println(j + " Para o contato " + vet[j].getNome());
		}
		posicao = sc.nextInt();
		vet[posicao] = null;
	}

	public static void pesquisar(Contatos[]vet) {

		System.out.println("Informe o nome do contato desejado ");
		String nome = sc.next();

		for (int j = 0; j < vet.length; j++) {
			if(vet[j] == null)continue;
			if(nome.equalsIgnoreCase(vet[j].getNome())) {
				System.out.println("Contato " + (j+1));
				System.out.println("Nome: " +  vet[j].getNome());
				System.out.println("Telefone: " + vet[j].getNome());
				System.out.println("Ano de nascimento: " + vet[j].getAnoNascimento());
				System.out.println("Data de nascimento: " + vet[j].getDataNasc());
				System.out.println();
			}
		}
	}
	static int menu(){
		int opc = 0;
		System.out.println("Escolha uma das op��es:\n [1]Incluir\n [2]Listar\n [3]Excluir\n [4]Pesquisar por nome\n [5]Finalizar");
		opc = sc.nextInt();
		if(opc == 5) {
			System.out.println("Programa finalizado ");
		}
		return opc;
	}


}
